<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-memoization?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// M
	'memoization_description' => 'Memoisation ist ein Begriffder Informatik, den Wikipédia so definiert: « Memoisation ist eine Technik, um Computerprogramme zu beschleunigen, indem Rückgabewerte von Funktionen zwischengespeichert anstatt neu berechnet werden. ». Es handelt sich ebenfals um den Namen einer PHP-Funktionsbibliothek, die das Ergebnis meiner Experimente mit XCache darstellt.',
	'memoization_slogan' => 'SPIP-Cache mit unterschiedlichen Methoden verwalten.',
];
