<?php
/**
 * Implementation methode Memoization
 *
 * @package SPIP\memoization\Memo
 **/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')){
	return;
}


class MCacheBackend_nocache implements MCacheBackend {

	public function init($params = null){
	}

	public function get($key){
		return null;
	}

	public function set($key, $value, $ttl = null){
		return null;
	}

	public function exists($key){
		return null;
	}

	public function del($key){
		return null;
	}

	public function inc($key, $value = null, $ttl = null){
		return null;
	}

	public function dec($key, $value = null, $ttl = null){
		return null;
	}

	public function lock($key, /* private */ $unlock = false){
		return null;
	}

	public function unlock($key){
		return null;
	}

	public function size(){
		return 0;
	}

	public function purge(){
		return false;
	}
}
