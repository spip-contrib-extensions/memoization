# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 4.0.0 - 2025-02-25

### Changed

- Compatible SPIP 4.2 minimum
- Chaînes de langue au format SPIP 4.1+

## 3.2.1 - 2024-08-01

### Added

- Composerisation du plugin
